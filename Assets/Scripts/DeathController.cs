﻿using UnityEngine;
using System.Collections;

public class DeathController : MonoBehaviour {

	private GameManager gameManager;

	void Start() {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.CompareTag("Player")) {
			gameManager.Death ();
		}
	}
}
