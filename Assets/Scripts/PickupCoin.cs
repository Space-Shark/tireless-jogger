﻿using UnityEngine;
using System.Collections;

public class PickupCoin : MonoBehaviour {

	public int pointsWorth;

	private ScoreManager scoreManager;

	private ObjectPooler coinPool;

	private AudioSource coinSound;

	// Use this for initialization
	void Start () {
		scoreManager = FindObjectOfType<ScoreManager> ();
		coinPool = FindObjectOfType<CoinGenerator> ().coinPool;
		coinSound = GameObject.Find ("CoinSFX").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.CompareTag ("Player")) {
			scoreManager.AddScore (pointsWorth);
			coinPool.PushPool (this.gameObject);

			if (coinSound.isPlaying)
				coinSound.Stop ();
			coinSound.Play ();
		}
	}
}
