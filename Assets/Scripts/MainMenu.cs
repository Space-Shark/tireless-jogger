﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public string playGameLevel;

	public EventSystem es;
	public GameObject firstSelected;

	public void PlayGame() {
		SceneManager.LoadScene (playGameLevel);
	}

	public void QuitGame() {
		Application.Quit ();
	}

	void Start() {
		es.SetSelectedGameObject (firstSelected);
	}

	void Update() {
		
	}
}
