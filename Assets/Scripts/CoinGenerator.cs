﻿using UnityEngine;
using System.Collections;

public class CoinGenerator : MonoBehaviour {

	public ObjectPooler coinPool;

	public float distanceBetweenCoins;

	public float coinSpawnThreshold;

	public void SpawnCoins(Vector3 startPosition) {
		GameObject coin = null;

		if (Random.Range (0f, 100f) < coinSpawnThreshold) {
			coin = coinPool.PopPool ();
			if (coin == null)
				return;
			coin.transform.position = startPosition;
		}

		if (Random.Range (0f, 100f) < coinSpawnThreshold) {
			coin = coinPool.PopPool ();
			if (coin == null)
				return;
			coin.transform.position = new Vector3 (startPosition.x - distanceBetweenCoins, startPosition.y, startPosition.z);
		}

		if (Random.Range (0f, 100f) < coinSpawnThreshold) {
			coin = coinPool.PopPool ();
			if (coin == null)
				return;
			coin.transform.position = new Vector3 (startPosition.x + distanceBetweenCoins, startPosition.y, startPosition.z);
		}
	}
}
