﻿using UnityEngine;
using System.Collections;

public class SpikeDestroyer : MonoBehaviour {

	public GameObject platformDestructionPoint;

	public ObjectPooler spikePool;

	// Use this for initialization
	void Start () {
		platformDestructionPoint = GameObject.Find ("PlatformDestructionPoint");
		spikePool = FindObjectOfType<SpikeGenerator> ().spikePool;
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x < platformDestructionPoint.transform.position.x) {
			spikePool.PushPool (this.gameObject);
		}
	}
}
