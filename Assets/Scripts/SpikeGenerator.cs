﻿using UnityEngine;
using System.Collections;

public class SpikeGenerator : MonoBehaviour {

	public ObjectPooler spikePool;

	public void SpawnSpikes(Vector3 startPosition, float width) {
		GameObject spike = spikePool.PopPool ();

		if (spike == null)
			return;

		float newPosX = startPosition.x + Random.Range (-width * 0.25f, width * 0.25f);
		spike.transform.position = new Vector3(newPosX, startPosition.y, startPosition.z);
	}
}
