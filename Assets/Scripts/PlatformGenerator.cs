﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {

	public CoinGenerator coinGenerator;
	public SpikeGenerator spikeGenerator;

	public Transform generationPoint;
	public float minDistanceBetween;
	public float maxDistanceBetween;

	private float baseHeight;
	public Transform maxHeightPoint;
	private float maxHeight;
	public float maxHeightChange;
	private float heightChange;

	public float randomCoinThreshold;
	public float randomSpikeThreshold;

	private float lastPlatformWidth;
	private float newPlatformWidth;

	public ObjectPooler platformPool;

	// Use this for initialization
	void Start () {
		coinGenerator = FindObjectOfType<CoinGenerator> ();
		spikeGenerator = FindObjectOfType<SpikeGenerator> ();
		lastPlatformWidth = 0.0f;

		baseHeight = transform.position.y;
		maxHeight = maxHeightPoint.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < generationPoint.transform.position.x) {
			GameObject platform = platformPool.PopPool ();
			newPlatformWidth = platform.GetComponent<BoxCollider2D> ().size.x;

			float distanceBetween = Random.Range (minDistanceBetween, maxDistanceBetween);

			float newHeight = transform.position.y + Random.Range (-maxHeightChange, maxHeightChange);
			newHeight = Mathf.Max (baseHeight, newHeight);
			newHeight = Mathf.Min (maxHeight, newHeight);

			transform.position = new Vector3 (transform.position.x + lastPlatformWidth/2 + newPlatformWidth/2 + distanceBetween, newHeight, transform.position.z);
			platform.transform.position = transform.position;

			if (Random.Range (0f, 100f) < randomCoinThreshold) {
				coinGenerator.SpawnCoins (new Vector3 (transform.position.x, transform.position.y + 0.5f, transform.position.z));
			}

			if (Random.Range (0f, 100f) < randomSpikeThreshold) {
				spikeGenerator.SpawnSpikes (new Vector3 (transform.position.x, transform.position.y + 0.5f, transform.position.z), newPlatformWidth);
			}

			lastPlatformWidth = newPlatformWidth;
		}
	}
}
