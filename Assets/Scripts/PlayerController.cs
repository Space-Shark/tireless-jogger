﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float speedMultiplier;

	public float speedIncreaseMilestone;
	private float speedMilestoneCount;

	public float jumpForce;
	private bool jumping;
	public float jumpTime;
	private float jumpTimeCounter;

	private Rigidbody2D myRigidBody;
	private Animator myAnimator;

	public GameManager gameManager;

	public bool grounded;
	public LayerMask whatIsGround;
	public Transform groundCheck;
	public float groundCheckRadius;

	public LayerMask whatIsDeath;
	private bool alive;

	private float baseMoveSpeed;
	private float baseSpeedMultiplier;
	private float baseSpeedIncreaseMilestone;

	public AudioSource jumpSound;
	public AudioSource deathSound;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent <Rigidbody2D>();
		myAnimator = GetComponent<Animator> ();

		baseMoveSpeed = moveSpeed;
		baseSpeedMultiplier = speedMultiplier;
		baseSpeedIncreaseMilestone = speedIncreaseMilestone;

		PlayerRestart ();
	}
	
	// Update is called once per frame
	void Update () {

		// You alive?
		if (alive) {
			// See if grounded
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);

			// Set speed
			if (transform.position.x > speedMilestoneCount) {
				speedMilestoneCount += speedIncreaseMilestone;
				speedIncreaseMilestone *= speedMultiplier;
				moveSpeed *= speedMultiplier;
			}
			myRigidBody.velocity = new Vector2 (moveSpeed, myRigidBody.velocity.y);

			// See if it can, and should, jump
			if (grounded && JumpButtonPressed()) {
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, jumpForce);
				jumping = true;
				jumpTimeCounter = jumpTime;
				jumpSound.Play ();
			}

			// Have they stopped holding the jump key down?
			if (JumpButtonReleased()) {
				jumping = false;
				jumpTimeCounter = 0;
			}

			// Long or short jump?
			if (jumping &&
				JumpButtonHeld() &&
				jumpTimeCounter > 0)
			{
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, jumpForce);
				jumpTimeCounter -= Time.deltaTime;
			}
		}

		// Make sure animations continue
		myAnimator.SetFloat ("Speed", moveSpeed);
		myAnimator.SetBool ("Grounded", grounded);
		myAnimator.SetBool ("Dead", !alive);

	}

	bool JumpButtonPressed() {
		return Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0) || Input.GetButtonDown ("Fire1") || Input.GetButtonDown ("A");
	}

	bool JumpButtonHeld() {
		return Input.GetKey (KeyCode.Space) || Input.GetMouseButton (0) || Input.GetButton ("Fire1") || Input.GetButton ("A");
	}

	bool JumpButtonReleased() {
		return Input.GetKeyUp (KeyCode.Space) || Input.GetMouseButtonUp (0) || Input.GetButtonUp ("Fire1") || Input.GetButtonUp ("A");
	}

	public void Die() {
		deathSound.Play ();

		alive = false;
		moveSpeed = 0.0f;
		myRigidBody.velocity = new Vector2 (moveSpeed, myRigidBody.velocity.y);
		myRigidBody.isKinematic = true;
	}

	public void PlayerRestart() {
		moveSpeed = baseMoveSpeed;
		speedMultiplier = baseSpeedMultiplier;
		speedIncreaseMilestone = baseSpeedIncreaseMilestone;

		speedMilestoneCount = speedIncreaseMilestone;

		jumpTimeCounter = jumpTime;
		jumping = false;
		alive = true;
		myRigidBody.isKinematic = false;
	}

}
