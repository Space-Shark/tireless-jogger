﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

	public GameObject[] objects;

	public int pooledAmount;

	private List<GameObject> pooledObjects;
	private List<GameObject> availableObjects;
	private List<GameObject> unavailableObjects;

	public ObjectPooler(GameObject[] objects, int pooledAmount) {
		this.objects = objects;
		this.pooledAmount = pooledAmount;
		this.Start ();
	}

	// Use this for initialization
	void Start () {
		pooledObjects = new List<GameObject> (pooledAmount);
		availableObjects = new List<GameObject> (pooledAmount);
		unavailableObjects = new List<GameObject> (pooledAmount);
		for (int i = 0; i < pooledAmount; i++) {
			GameObject obj = (GameObject)Instantiate (objects [Random.Range (0, objects.Length)]);
			obj.SetActive(false);
			pooledObjects.Add (obj);
			availableObjects.Add (obj);
		}
	}

	public void PushPool(GameObject obj) {
		if (unavailableObjects.Remove (obj)) {
			availableObjects.Add (obj);
			obj.SetActive (false);
		}
	}

	public GameObject PopPool() {
		if (availableObjects.Count <= 0)
			return null;
		int idx = Random.Range(0,availableObjects.Count);
		GameObject obj = availableObjects [idx];
		if (availableObjects.Remove (obj)) {
			unavailableObjects.Add(obj);
			obj.SetActive(true);
			return obj;
		}
		return null;
	}

	public void ResetPool() {
		foreach (GameObject obj in pooledObjects) {
			PushPool (obj);
		}
	}
}
