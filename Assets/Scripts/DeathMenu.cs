﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class DeathMenu : MonoBehaviour {

	public string mainMenuLevel;

	public Button firstSelected;

	public void RestartGame() {
		FindObjectOfType<GameManager> ().RestartGame ();
	}

	public void QuitToMainMenu() {
		SceneManager.LoadScene (mainMenuLevel);
	}
}
