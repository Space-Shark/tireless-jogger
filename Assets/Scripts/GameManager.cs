﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

	private PlatformGenerator platformGenerator;
	private Vector3 platformStartPoint;

	private PlayerController player;
	private Vector3 playerStartPoint;

	private ScoreManager scoreManager;

	public DeathMenu deathMenu;
	public PauseMenu pauseMenu;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		platformGenerator = FindObjectOfType<PlatformGenerator> ();
		scoreManager = FindObjectOfType<ScoreManager> ();
		platformStartPoint = platformGenerator.transform.position;
		playerStartPoint = player.transform.position;
		deathMenu.gameObject.SetActive (false);
	}

	public void Death() {
		scoreManager.GameOver ();
		player.Die ();
		deathMenu.gameObject.SetActive (true);
		pauseMenu.gameObject.SetActive (false);
		deathMenu.firstSelected.Select ();
	}

	public void RestartGame() {
		// No longer using coroutine
		//StartCoroutine ("RestartGameCo");

		platformGenerator.platformPool.ResetPool ();
		platformGenerator.coinGenerator.coinPool.ResetPool ();
		platformGenerator.spikeGenerator.spikePool.ResetPool ();

		player.transform.position = playerStartPoint;
		platformGenerator.transform.position = platformStartPoint;
		scoreManager.Reset ();
		player.PlayerRestart();
		deathMenu.gameObject.SetActive (false);
		pauseMenu.gameObject.SetActive (true);
	}

	/*
	public IEnumerator RestartGameCo() {
		scoreManager.GameOver ();
		player.Die ();
		
		yield return new WaitForSeconds (1.5f);

		platformGenerator.platformPool.ResetPool ();
		platformGenerator.coinGenerator.coinPool.ResetPool ();

		player.transform.position = playerStartPoint;
		platformGenerator.transform.position = platformStartPoint;
		scoreManager.Reset ();
		player.PlayerRestart();
	}
	*/
}
