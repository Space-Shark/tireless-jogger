﻿using UnityEngine;
using System.Collections;

public class PlatformDestroyer : MonoBehaviour {

	public GameObject platformDestructionPoint;

	public ObjectPooler platformPool;

	// Use this for initialization
	void Start () {
		platformDestructionPoint = GameObject.Find ("PlatformDestructionPoint");
		platformPool = FindObjectOfType<PlatformGenerator> ().platformPool;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < platformDestructionPoint.transform.position.x) {
			platformPool.PushPool (this.gameObject);
		}
	}
}
