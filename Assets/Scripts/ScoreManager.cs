﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public Text scoreText;
	public Text highScoreText;
	public Text gameOverText;

	public float score;
	public float highScore;
	private bool newHighScore;

	private float pointsPerSecond;
	public float basePointsPerSecond;

	public bool scoreIncreasing;

	// Use this for initialization
	void Start () {
		if (basePointsPerSecond == 0)
			basePointsPerSecond = 1;
		pointsPerSecond = basePointsPerSecond;
		Reset ();
		if (PlayerPrefs.HasKey("highScore")) {
			highScore = PlayerPrefs.GetFloat ("highScore");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (scoreIncreasing) {
			score += Time.deltaTime * pointsPerSecond;
		}

		newHighScore = (score > highScore);

		if (newHighScore) {
			highScore = score;
			PlayerPrefs.SetFloat ("highScore", highScore);
		}

		UpdateScoreText ();
	}

	public void Reset() {
		// Reset stuff
		score = 0.0f;
		pointsPerSecond = basePointsPerSecond;
		gameOverText.text = "";
		scoreIncreasing = true;
	}

	public void AddScore(int addScore) {
		score += addScore;
	}

	string GetScore() {
		return Mathf.RoundToInt(score).ToString ();
	}

	string GetHighScore() {
		return Mathf.RoundToInt(highScore).ToString ();
	}

	void UpdateScoreText() {
		scoreText.text = "Score: " + GetScore ();
		highScoreText.text = "High Score: " + GetHighScore ();
	}

	public void GameOver() {
		scoreIncreasing = false;
		string gameOverMessage = "Game Over\nScore: " + GetScore ();
		if (newHighScore) {
			gameOverMessage += "\nNew High Score!";
		}
		gameOverText.text = gameOverMessage;

	}
}
