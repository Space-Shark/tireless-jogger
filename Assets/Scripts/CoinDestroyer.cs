﻿using UnityEngine;
using System.Collections;

public class CoinDestroyer : MonoBehaviour {

	public GameObject platformDestructionPoint;

	public ObjectPooler coinPool;

	// Use this for initialization
	void Start () {
		platformDestructionPoint = GameObject.Find ("PlatformDestructionPoint");
		coinPool = FindObjectOfType<CoinGenerator> ().coinPool;
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x < platformDestructionPoint.transform.position.x) {
			coinPool.PushPool (this.gameObject);
		}
	}
}
